//
//  IGMWidgets.h
//  IGMUtilities
//
//  Created by Pa on 19/11/15.
//  Copyright © 2015 Ingram Micro. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGMWidgets : NSObject

- (void)sampleMethod;

@end
