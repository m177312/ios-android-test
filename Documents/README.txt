The App Ingram Micro loads football data using the provided url. It is a universal application supporting multiple orientations. It will compatible with iOS 8 and up. The app uses auto layout and universal storyboards. The app is coded in objective-c.

The project uses cocoa pods. More here : https://guides.cocoapods.org/using/getting-started.html

Kindly install the pod file found in /Projects/Ingram Micro/

The match fixtures are displayed in the home page. 
The user may tap on the season to get information on the same.
The user may also tap of a team to get more information.
In the team page, the user may tap on the players button to get the list of players in a team.

Note : The svg images may not load on the iOS simulator.